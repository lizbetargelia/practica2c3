const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");
const db = require("../models/alumno.js");

let alumno = {
    matricula:"",
    nombre: "",
    domicilio: "",
    sexo: "",
    especialidad:""
    
}

router.get("/",(req,res)=>{
    const valores = {
        matricula:req.query.matricula,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        sexo:req.query.sexo,
        especialidad:req.query.especialidad
    }
    res.render('alumnos.html',valores);

});



//insertar un alumno
router.post("/insertar",async(req,res)=>{
    alumno = {
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
       sexo:req.body.sexo,
        especialidad:req.body.especialidad
        
    }
    let resultado = await db.insertar(alumno);
    res.json(resultado);
})



//mostrar todos
router.get('/mostrarTodos', async(req,res)=>{
    resultado = await db.mostrarTodos();
    res.json(resultado);
})

//opcion 1 buscar alumno por matricula
router.get('/buscarAlumno',(req,res)=>{
    const matricula = req.query.matricula;
    db.buscarAlumno(matricula).then((data)=> res.json(data)).catch((err)=> res.status(404).send(err.message));
})

//opcion 2 buscar alumno por matricula
//router.get('/buscarAlumno/:matricula', async(req,res)=>{
//    matricula = req.params.matricula
//    resultado = await db.buscarAlumno(matricula);
//    res.json(resultado);
//})

//eliminar
router.delete('/borrarMatricula', async(req,res)=>{
    const matricula = req.query.matricula;
    db.borrarMatricula(matricula).then((data)=> res.json(data)).catch((err)=> res.status(404).send(err.message));
})

//actualizar
router.put('/actualizar', async(req,res)=>{
    const alumno = req.body;
    db.actualizar(alumno).then((data)=> res.json(data)).catch((err)=> res.status(404).send(err.message));
})

router.post("/",(req,res)=>{
    const valores = {
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        sexo:req.body.sexo,
        especialidad:req.body.especialidad
    }
    res.render('alumnos.html',valores);

});

module.exports = router;