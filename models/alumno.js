const json = require('express/lib/response');

const promise = require("../models/conexion.js");
const conexion = require("../models/conexion.js");

var AlumnoDB = {}


AlumnoDB.insertar = function insertar(alumno){
    return new Promise ((resolve,reject)=>{
        
        var sqlConsulta = "Insert into alumno set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                resolve({
                    id:res.insertId, 
                    matricula:alumno.matricula,
                    nombre:alumno.nombre, 
                    domicilio:alumno.domicilio,
                    sexo:alumno.sexo,
                    especialidad:alumno.especialidad

                })
            }

        });
    });
}



AlumnoDB.mostrarTodos = function mostrarTodos(){
     alumno = {};
    return new Promise ((resolve,reject)=>{
        
        var sqlConsulta = "select * from alumno";
        conexion.query(sqlConsulta,null, function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                alumno=res;

                resolve(alumno)
            }

        });
    });
}

/*buscar por matricula */
AlumnoDB.buscarAlumno = function buscarAlumno(matricula){
    alumno = {};
    return new Promise ((resolve,reject)=>{
        var sqlConsulta = "select * from alumno where matricula = ?";
        conexion.query(sqlConsulta,[matricula], function(err,res){
            if(err){
                reject(err.message);
            }
            else if(res.length<=0){
                reject({message:'No existe'});
                
            }else{
                resolve(res);
            } 

        });
    });
}
/*borrar por matricula */
AlumnoDB.borrarMatricula = function borrarMatricula(matricula){
    return new Promise((resolve,reject)=>{
        var sqlConsulta = "delete from alumno where matricula = ?";
        conexion.query(sqlConsulta,[matricula], function(err,res){
            if(err){

                reject(err.message);
            }
            else{
                
                resolve(res.affectedRows)
            }
        });
    });
}
/* Actualizar alumno */
AlumnoDB.actualizar = function actualizar(alumno){
    return new Promise ((resolve,reject)=>{
        
        var sqlConsulta = "update alumno set nombre = ?, domicilio = ?, sexo = ?, especialidad = ? where matricula = ?";
        conexion.query(sqlConsulta,[alumno.nombre, alumno.domicilio,alumno.sexo,alumno.especialidad, alumno.matricula],function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                resolve(res.affectedRows)
            }

        });
    });
        
    
}
module.exports = AlumnoDB;